import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-price',
  templateUrl: './price.component.html',
  styleUrls: ['./price.component.css']
})
export class PriceComponent implements OnInit {

  constructor() { }

  public getToolTipValue(): string {
    return "Avg.price: 000.764768 \n Sum BTC: 345678 \n Sum USDT : 45677788888899";
  } 
  
  ngOnInit() {
  }

}
