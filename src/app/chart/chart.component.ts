import { Component, OnInit } from '@angular/core';

declare var $: any;
declare function loadChart(): any;

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    
    // var script = document.createElement('script');

    // script.setAttribute('src','https://s3.tradingview.com/tv.js?cb=loadChart');

    // document.head.appendChild(script);

    loadChart();
  }


}
