import { Component, OnInit } from '@angular/core';

declare function togglePassword(): any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

 
  constructor() { }


  showpass = () => {       
    console.log("show password"); 
    togglePassword(); 
       
  }  
  
  ngOnInit() {
  }

}
