import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {TooltipModule} from 'primeng/tooltip';
import { FormsModule } from '@angular/forms';
import {CalendarModule} from 'primeng/calendar';
// import { ShowHidePasswordModule } from 'ngx-show-hide-password';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ChartComponent } from './chart/chart.component';
import { PriceComponent } from './price/price.component';
import { LoginComponent } from './login/login.component';
import { FooterComponent } from './footer/footer.component';
import { HeadComponent } from './head/head.component';
import { AdvancedTradeComponent } from './advanced-trade/advanced-trade.component';
// import {TooltipModule} from 'ng2-tooltip-directive';
import { OrdersComponent } from './orders/orders.component';
import { MarginExchangeComponent } from './margin-exchange/margin-exchange.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { RegisterComponent } from './register/register.component';
import {ChartModule} from 'primeng/chart';
import { ShowHideDirective } from './show-hide.directive';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ChartComponent,
    PriceComponent,
    LoginComponent,
    FooterComponent,
    HeadComponent,
    AdvancedTradeComponent,
    OrdersComponent,
    MarginExchangeComponent,
    DashboardComponent,
    ProfileComponent,
    RegisterComponent,
    ShowHideDirective
  ],
  imports: [
    TooltipModule,
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    CalendarModule,
    ChartModule
    // ShowHidePasswordModule    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
