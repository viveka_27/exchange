import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvancedTradeComponent } from './advanced-trade.component';

describe('AdvancedTradeComponent', () => {
  let component: AdvancedTradeComponent;
  let fixture: ComponentFixture<AdvancedTradeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdvancedTradeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdvancedTradeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
