import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarginExchangeComponent } from './margin-exchange.component';

describe('MarginExchangeComponent', () => {
  let component: MarginExchangeComponent;
  let fixture: ComponentFixture<MarginExchangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarginExchangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarginExchangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
