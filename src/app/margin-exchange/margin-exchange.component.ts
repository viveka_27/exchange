import { Component, OnInit , Input } from '@angular/core';

@Component({
  selector: 'app-margin-exchange',
  templateUrl: './margin-exchange.component.html',
  styleUrls: ['./margin-exchange.component.css']
})
export class MarginExchangeComponent implements OnInit {

  constructor() { }
  //@Input() moveTab: string;
  moveTab="true";

  onStopLimit(){       
    console.log("Stop Limit");    
    this.moveTab ='false';
  }  
  onLimit(){       
    console.log("Limit");    
    this.moveTab ='true';
  }  
  onMarket(){       
    console.log("Market");    
    this.moveTab ='true';
  }    

  ngOnInit() {
  }

}
